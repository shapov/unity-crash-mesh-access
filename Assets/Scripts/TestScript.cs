﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    MeshFilter[] _meshFilters;
    Bounds _bounds;

    protected virtual void Awake() {
        Transform prefab = Resources.Load<Transform>("Prefabs/Storage");
        Transform instance = Instantiate(prefab);

        _meshFilters = instance.GetComponentsInChildren<MeshFilter>();
        _bounds = GetMeshBounds();
    }

    protected Bounds GetMeshBounds() {
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
        foreach (MeshFilter filter in _meshFilters) {
            bounds.Encapsulate(filter.mesh.bounds);
        }
        return bounds;
    }
}
